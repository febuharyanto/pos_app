import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pos/screens/sign_in.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import '../cart/cart_screen.dart';
import '../utils/constants.dart';
import '../widget/bottomBar.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey(debugLabel: "Main Navigator");

  TextEditingController controllerNominal = TextEditingController();
  TextEditingController controllerLiter = TextEditingController();
  TextEditingController controllerPic = TextEditingController();
  TextEditingController controllerKm = TextEditingController();
  TextEditingController controllerJenisPembayaran = TextEditingController();
  TextEditingController controllerLokasiPengisian = TextEditingController();
  TextEditingController controllerLat = TextEditingController();
  TextEditingController controllerLng = TextEditingController();

  bool isLoading = false;
  var fullname = '';
  var email = '';
  var urlPlaystore = '';

  var isPermission = false;

  @override
  void dispose() {
    super.dispose();
  }

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later

  void initState() {
    super.initState();
    (() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        email = prefs.getString('email').toString();
        fullname = prefs.getString('name').toString();
      });
    })();
  }

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      // developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  _updateVerisonDialog() {
    return AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.RIGHSLIDE,
            headerAnimationLoop: false,
            title: 'Update Versi Aplikasi',
            desc: 'Silakan Update versi terbaru di Playstore',
            btnOkText: 'Update Sekarang',
            btnOkOnPress: () {
              // _launchURL(urlPlaystore);
            },
            btnOkColor: blue3)
        .show();
  }

  _logout() async {
    final pref = await SharedPreferences.getInstance();
    print('pref ' + pref.getString('email').toString());

    await pref.clear();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => SignIn(),
        ),
        (Route<dynamic> route) => false
        // ModalRoute.withName('/')
        );
  }

  _logoutConfirmation() {
    AwesomeDialog(
            context: context,
            headerAnimationLoop: false,
            dialogType: DialogType.WARNING,
            animType: AnimType.BOTTOMSLIDE,
            title: 'LOGOUT',
            desc: 'Are You Sure ?',
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              _logout();
            },
            btnOkText: 'YES',
            btnCancelText: 'NO')
        .show();
  }

  int _is_online = 0;

  int page = 0;
  int limit = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomBar(0),
        appBar: PreferredSize(
          preferredSize:
              Size.fromHeight(MediaQuery.of(context).size.height * 0.08),
          child: AppBar(
            elevation: 0,
            backgroundColor: blue3,
            centerTitle: true,
            automaticallyImplyLeading: false,
            title: Row(
              //crossAxisAlignment: CrossAxisAlignment.s,
              children: [
                InkWell(
                  onTap: () {
                    // Navigator.of(context)
                    //     .push(new MaterialPageRoute(builder: (_) {
                    //   return new Profile();
                    // }));
                  },
                  child: Image(
                    image: AssetImage("assets/assets_monitor.png"),
                    width: 45,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      " Welcome",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' ' + fullname,
                      style: TextStyle(
                          fontFamily: "SourceSerifLight",
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ],
                )
              ],
            ),
            actions: [
              Container(
                  margin: EdgeInsets.all(10),
                  child: IconButton(
                      onPressed: () {
                        _logoutConfirmation();
                      },
                      icon: Icon(Icons.logout)))
            ],
          ),
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: <Widget>[
            // Padding(padding: EdgeInsets.only(top: 10)),
            Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: new BoxDecoration(
                    color: blue3,
                    boxShadow: [new BoxShadow(blurRadius: 5.0)],
                    borderRadius: new BorderRadius.vertical(
                        bottom: new Radius.elliptical(
                            MediaQuery.of(context).size.width, 300.0)),
                  ),
                ),
                Align(
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.center,
                    // width: MediaQuery.of(context).size.width * 0.9,
                    // height: MediaQuery.of(context).size.height * 0.1,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        new BoxShadow(blurRadius: 5.0, color: Colors.grey)
                      ],
                      borderRadius: new BorderRadius.circular(20),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(left: 10),
                                // width: MediaQuery.of(context).size.width * 0.1,
                                // height: MediaQuery.of(context).size.width * 0.1,
                                decoration: new BoxDecoration(
                                  // color: Colors.blue[50],
                                  //boxShadow: [new BoxShadow(blurRadius: 0.0)],
                                  borderRadius: new BorderRadius.circular(10),
                                ),
                                child: new Image.asset(
                                  "assets/transaction.png",
                                  height: 30.0,
                                  width: 30.0,
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Transaksi Hari Ini",
                                    style: TextStyle(
                                        //fontFamily: "SourceSeriflight",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Colors.grey[500]),
                                  ),
                                  Text(
                                    " ",
                                    style: TextStyle(
                                        fontFamily: "Rowdies",
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Flexible(
                          flex: 5,
                          child: Row(
                            children: [
                              Container(
                                // color: Colors.grey,
                                alignment: Alignment.center,
                                // width: MediaQuery.of(context).size.width * 0.1,
                                // height: MediaQuery.of(context).size.width * 0.1,
                                decoration: new BoxDecoration(
                                  // color: Colors.blue[50],
                                  //boxShadow: [new BoxShadow(blurRadius: 0.0)],
                                  borderRadius: new BorderRadius.circular(10),
                                ),
                                child: new Image.asset(
                                  "assets/shopping-bag.png",
                                  height: 30.0,
                                  width: 30.0,
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      " Customer Hari Ini ",
                                      style: TextStyle(
                                          //fontFamily: "SourceSeriflight",
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10,
                                          color: Colors.grey[500]),
                                    ),
                                    Row(children: [
                                      Text(
                                        " ",
                                        style: TextStyle(
                                            fontFamily: "Rowdies",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      ),
                                    ]),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),

            Container(
              decoration: BoxDecoration(
                  // color: Colors.blueGrey,
                  border: Border.all(
                    color: blue3,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.all(
                    new Radius.circular(6.0),
                  )
                  // borderRadius:
                  //     BorderRadius.all(new Radius.circular(6.0))
                  ),
              margin: EdgeInsets.only(left: 15.0, right: 15.0),
              padding:
                  EdgeInsets.only(left: 16, top: 24, bottom: 20, right: 20),
              // width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height * 0.15,
              constraints: BoxConstraints(
                maxHeight: double.infinity,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ResponsiveGridRow(
                    children: [
                      ResponsiveGridCol(
                        xs: 3,
                        md: 3,
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .push(new MaterialPageRoute(builder: (_) {
                              return new CartScreen();
                            }));

                            // Navigator.pushNamed(context, CartScreen.routeName);
                          },
                          child: Column(children: [
                            Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(10),
                              ),
                              child: new Image.asset(
                                "assets/qr-code.png",
                                height: 40.0,
                                width: 40.0,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text("Scan Barcode",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold))
                          ]),
                        ),
                      ),
                      ResponsiveGridCol(
                        xs: 3,
                        md: 3,
                        child: InkWell(
                          onTap: () {},
                          child: Column(children: [
                            Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(10),
                              ),
                              child: new Image.asset(
                                "assets/products.png",
                                height: 40.0,
                                width: 40.0,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text("Products",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold))
                          ]),
                        ),
                      ),
                      ResponsiveGridCol(
                        xs: 3,
                        md: 3,
                        child: InkWell(
                          onTap: () {},
                          child: Column(children: [
                            Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(10),
                              ),
                              child: new Image.asset(
                                "assets/stock.png",
                                height: 40.0,
                                width: 40.0,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text("Stock",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold))
                          ]),
                        ),
                      ),
                      ResponsiveGridCol(
                        xs: 3,
                        md: 3,
                        child: InkWell(
                          onTap: () {},
                          child: Column(children: [
                            Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(10),
                              ),
                              child: new Image.asset(
                                "assets/promo.png",
                                height: 40.0,
                                width: 40.0,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text("Promo",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold))
                          ]),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),

            Container(
              padding: EdgeInsets.only(left: 15.0, right: 5.0),
              child: Row(
                children: <Widget>[
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 10,
                    child: Column(
                      children: [
                        Container(
                          // color: Colors.te,
                          decoration: BoxDecoration(
                              color: blue3,
                              border: Border.all(
                                color: blue3,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.only(
                                  topLeft: new Radius.circular(6.0),
                                  topRight: new Radius.circular(6.0))
                              // borderRadius:
                              //     BorderRadius.all(new Radius.circular(6.0))
                              ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.history,
                                  size: 30,
                                  color: Colors.white,
                                ),
                                Text(
                                  ' Riwayat Transaksi',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: blue3,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: new Radius.circular(6.0),
                                  bottomRight: new Radius.circular(6.0))
                              // borderRadius:
                              //     BorderRadius.all(new Radius.circular(6.0))
                              ),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(15),
                                // height: 160.0,
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Container(
                                            decoration: new BoxDecoration(
                                              borderRadius:
                                                  new BorderRadius.circular(10),
                                            ),
                                            child: new Image.asset(
                                              "assets/transaction.png",
                                              height: 30.0,
                                              width: 30.0,
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "  Date ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12,
                                                    color: Colors.grey[500]),
                                              ),
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: "Rowdies",
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "  Building Name ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12,
                                                    color: Colors.grey[500]),
                                              ),
                                              Row(children: [
                                                Text(
                                                  "  ",
                                                  style: TextStyle(
                                                      fontFamily: "Rowdies",
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black),
                                                ),
                                              ]),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            decoration: new BoxDecoration(
                                              borderRadius:
                                                  new BorderRadius.circular(10),
                                            ),
                                            child: IconButton(
                                              onPressed: () {},
                                              icon: Icon(Icons
                                                  .keyboard_arrow_right_rounded),
                                              color: darkBlue,
                                              iconSize: 40,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Divider(color: Colors.black),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(bottom: 15),
                                child: FloatingActionButton.extended(
                                  heroTag: 'btn1',
                                  onPressed: () {},
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0))),
                                  backgroundColor: blue3,
                                  label: Row(
                                    children: [
                                      const Text(
                                        "View More",
                                        style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Icon(
                                        Icons.arrow_right_rounded,
                                        color: Colors.white,
                                        size: 40,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 10.0),
                  ),
                ],
              ),
            ),
          ],
        )

        //
        );
  }
}

typedef OnPickImageCallback = void Function(
    double? maxWidth, double? maxHeight, int? quality);
