import 'dart:async';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:pos/component/background.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

import '../utils/fonts.dart';
import '../widget/appbutton.dart';
import '../widget/inputtext.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  TextEditingController controllerEmailLogin = TextEditingController();
  TextEditingController controllerPasswordLogin = TextEditingController();
  TextEditingController _emailBoxController = TextEditingController();

  SharedPreferences? sharedPreferences;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  var email = '';

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  _button_offline(provider_status) {
    if (email != null || email != '') {
      print("email " + email);
      return Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: [
            SizedBox(
              child: Text("Anda berada dalam mode offline",
                  // provider.status,
                  style: TextStyle(fontSize: 14.0, color: Colors.black)),
            ),
            SizedBox(
              height: 25.0,
            ),
            InputText(
              text: "Email",
              controllerText: controllerEmailLogin,
              typeInputText: 'email',
            ),
            SizedBox(height: 25.0),
            AppButton(
              text: "Submit Offline",
              onPressed: () {
                if (controllerEmailLogin.text == '') {
                  AwesomeDialog(
                          context: context,
                          dialogType: DialogType.WARNING,
                          animType: AnimType.RIGHSLIDE,
                          headerAnimationLoop: false,
                          title: 'Failed',
                          desc: 'Silakan lengkapi email Anda',
                          btnOkOnPress: () {},
                          btnOkColor: Colors.red)
                      .show();
                } else {
                  // Navigator.of(context)
                  //     .push(new MaterialPageRoute(builder: (_) {
                  //   return new HomeOffline(controllerEmailLogin.text);
                  // }));
                }
              },
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );
    } else {
      print("email2 " + email);
      return Container(
        child: Column(
          children: [
            InputText(
              text: "Email",
              controllerText: controllerEmailLogin,
              typeInputText: 'email',
            ),
            SizedBox(height: 25.0),
            InputText(
              text: "Password",
              controllerText: controllerPasswordLogin,
              typeInputText: 'password',
            ),
            SizedBox(
              height: 35.0,
            ),
            AppButton(
              text: "Log In",
              onPressed: () {
                // _prosesLogin(provider_status, controllerEmailLogin.text);
              },
            ),
            SizedBox(
              height: 25.0,
            ),
            SizedBox(
              child: Text("Anda berada dalam mode offline",
                  // provider.status,
                  style: TextStyle(fontSize: 14.0, color: Colors.white)),
            ),
          ],
        ),
      );
    }
  }

  _button_online() {
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        children: [
          InputText(
            text: "Email",
            controllerText: controllerEmailLogin,
            typeInputText: 'email',
          ),
          SizedBox(height: 25.0),
          InputText(
            text: "Password",
            controllerText: controllerPasswordLogin,
            typeInputText: 'password',
          ),
          SizedBox(
            height: 35.0,
          ),
          AppButton(
            text: "Log In",
            onPressed: () {
              // _prosesLogin(provider_status, controllerEmailLogin.text);
            },
          ),
          SizedBox(
            height: 35.0,
          ),
          SizedBox(
            child: InkWell(
              onTap: () {},
              child: Text("OFFLINE MODE",
                  // provider.status,
                  style: TextStyle(fontSize: 14.0, color: Colors.white)),
            ),
          ),
          // SizedBox(
          //   height: 25.0,
          // ),
        ],
      ),
    );
  }

  Timer? timer;

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  var _path = '';
  @override
  void initState() {
    super.initState();
    _checkPlatform();
    super.initState();
    initConnectivity();

    (() async {
      setState(() {});
    })();
  }

  _checkPlatform() {
    if (Platform.isAndroid) {
      setState(() {
        _path = '/data/user/0/com.bmm.moratelindo/app_flutter/';
      });
      // Android-specific code
    } else if (Platform.isIOS) {
      // iOS-specific code
      setState(() {
        _path =
            '/var/mobile/Containers/Data/Application/867139DC-E3C9-407D-8FBD-9AB3B9EE6839/Documents/';
      });
    }
  }

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      // developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (mounted) {
      setState(() {
        _connectionStatus = result;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final forgotPassword = Text("Lupa Password ?",
        textAlign: TextAlign.left,
        style: Fonts.style.copyWith(
            color: Colors.cyan[200],
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
            fontSize: 14.0));
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Background(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // SizedBox(height: 35.0),
              SizedBox(
                height: 160.0,
                child: Image.asset(
                  "assets/assets_monitor.png",
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(height: 15.0),
              Padding(
                padding: const EdgeInsets.only(right: 56, left: 56),
                child: SizedBox(
                  child: Center(
                    child: Text('Point Of Sales',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0,
                            color: Colors.black)),
                  ),
                ),
              ),
              // SizedBox(height: 15.0),
              // Container(
              //   alignment: Alignment.centerLeft,
              //   padding: EdgeInsets.symmetric(horizontal: 0),
              //   child: SizedBox(
              //     height: 160.0,
              //     child: Image.asset(
              //       "assets/building.png",
              //       fit: BoxFit.contain,
              //     ),
              //   ),
              // ),
              // Container(
              //   alignment: Alignment.centerLeft,
              //   padding: EdgeInsets.symmetric(horizontal: 40),
              //   child: Text(
              //     "BMM",
              //     style: TextStyle(
              //         fontWeight: FontWeight.bold,
              //         color: Color(0xFF2661FA),
              //         fontSize: 36),
              //     textAlign: TextAlign.left,
              //   ),
              // ),

              SizedBox(
                height: 35.0,
              ),
              _button_online(),
              SizedBox(
                child: Text('Developed By Team',
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold)),
              ),
              SizedBox(
                height: 35.0,
              ),

              // Container(
              //   alignment: Alignment.center,
              //   margin: EdgeInsets.symmetric(horizontal: 40),
              //   child: TextField(
              //     decoration: InputDecoration(labelText: "Username"),
              //   ),
              // ),
              // SizedBox(height: size.height * 0.03),
              // Container(
              //   alignment: Alignment.center,
              //   margin: EdgeInsets.symmetric(horizontal: 40),
              //   child: TextField(
              //     decoration: InputDecoration(labelText: "Password"),
              //     obscureText: true,
              //   ),
              // ),
              // Container(
              //   alignment: Alignment.centerRight,
              //   margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              //   child: Text(
              //     "Forgot your password?",
              //     style: TextStyle(fontSize: 12, color: Color(0XFF2661FA)),
              //   ),
              // ),
              // SizedBox(height: size.height * 0.05),
              // Container(
              //   alignment: Alignment.centerRight,
              //   margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              //   child: ElevatedButton(
              //     onPressed: () {},
              //     child: Container(
              //       alignment: Alignment.center,
              //       height: 50.0,
              //       width: size.width * 0.5,
              //       decoration: new BoxDecoration(
              //           borderRadius: BorderRadius.circular(80.0),
              //           gradient: new LinearGradient(colors: [
              //             Color.fromARGB(255, 255, 136, 34),
              //             Color.fromARGB(255, 255, 177, 41)
              //           ])),
              //       padding: const EdgeInsets.all(0),
              //       child: Text(
              //         "LOGIN",
              //         textAlign: TextAlign.center,
              //         style: TextStyle(fontWeight: FontWeight.bold),
              //       ),
              //     ),
              //   ),
              // ),
              // Container(
              //   alignment: Alignment.centerRight,
              //   margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              //   child: GestureDetector(
              //     onTap: () => {},
              //     child: Text(
              //       "Don't Have an Account? Sign up",
              //       style: TextStyle(
              //           fontSize: 12,
              //           fontWeight: FontWeight.bold,
              //           color: Color(0xFF2661FA)),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
