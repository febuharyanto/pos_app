import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pos/screens/home.dart';
import 'package:pos/screens/sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info/package_info.dart';

class Splashscreen extends StatefulWidget {
  const Splashscreen({Key? key}) : super(key: key);

  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  SharedPreferences? sharedPreferences;
  bool islogin = false;
  var id = '';
  var email = '';
  var api_token = '';

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      print('App name ' + _packageInfo.appName);
      print('Package name ' + _packageInfo.packageName);
      print('App version ' + _packageInfo.version);
      print('Build number ' + _packageInfo.buildNumber);
    });
  }

  Future<void> initializePreference() async {
    this.sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    _initPackageInfo();
    super.initState();
    (() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      // initializePreference().whenComplete(() {
      print('department ' + prefs.getString("department").toString());
      setState(() {
        if (prefs.getBool("isLogin") != null) {
          id = prefs.getString('sso_id').toString();
          email = prefs.getString('email').toString();
          api_token = prefs.getString('api_token').toString();
          islogin = true;
          startLaunching(id, api_token);
        } else {
          startLaunching2();
        }
      });
      // });
    })();
  }

  startLaunching2() async {
    var duration = const Duration(seconds: 2);
    return new Timer(duration, () {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        return new Home();
      }));
    });
  }

  startLaunching(id, api_token) async {
    var duration = const Duration(seconds: 2);

    return new Timer(duration, () {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        return new Home();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Align(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Image(
                image: AssetImage('assets/assets_monitor.png'),
                fit: BoxFit.contain,
                height: 200,
              ),
              Padding(
                padding: EdgeInsets.only(top: 25.0),
              ),
              Text(
                'POS APP',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20.0),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              Text(
                "App Version " + _packageInfo.version,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                    fontSize: 11.0),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5.0),
              ),
              Text(
                "Developed By Team",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                    fontSize: 11.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
