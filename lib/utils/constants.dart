import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

final Color primaryColor = Colors.black;
final Color secondaryColor = Colors.blue[900]!;
final Color indigo = Colors.indigo[900]!;
final Color cyan = Colors.cyan[200]!;
final Color indigo600 = Colors.indigo[600]!;
final Color blue = Colors.blue[800]!;
final Color darkBlue = HexColor("#2a5299");
final Color body = HexColor("#062949");
final Color button = HexColor("#53a0eb");
final Color list = HexColor("#0774ae");
final Color bluelogo = HexColor("#7FB8E7");
final Color blue2 = HexColor("#247bee");
final Color blue3 = HexColor("#5495ef");
final Color blue4 = HexColor("#85b1ee");
final Color grey = HexColor("#d5e1ef");

final kMainColor = Color(0xFFFF7643);
final kMainLightColor = Color(0xFFFFECDF);
final kMainGradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
final kSecondaryColor = Color(0xFF979797);
final kTextColor = Color(0xFF757575);

final kAnimateTime = Duration(milliseconds: 200);

final headingStyleText = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

final time = Duration(milliseconds: 250);
//error statements
final RegExp kValidEmail = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final String kNoMail = "Please Enter your email";
final String kWrongEmailFormat = "Please Enter Valid Email";
final String kNoPass = "Please Enter your password";
final String kWeakPass = "Password is too short";
final String kPassMismatch = "Passwords don't match";
final String kNoName = "Please Enter your name";
final String kNoNumber = "Please Enter your phone number";
final String kNoAddress = "Please Enter your address";

final otpStyle = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 15),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(15),
    borderSide: BorderSide(color: kTextColor),
  );
}
