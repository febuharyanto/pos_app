import 'package:flutter/material.dart';

class Fonts {
  static final TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.white);
}
