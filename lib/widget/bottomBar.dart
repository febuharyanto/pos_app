import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pos/screens/sign_in.dart';
import 'package:pos/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screens/home.dart';

class BottomBar extends StatefulWidget {
  BottomBar(this.idx);
  final int idx;

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();
  int _selectedIndex = 0;
  SharedPreferences? sharedPreferences;

  _logout() async {
    sharedPreferences?.clear();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => SignIn(),
        ),
        ModalRoute.withName('/'));
  }

  var sso_id = '';
  var api_token = '';

  @override
  void initState() {
    super.initState();

    (() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      // sharedPreferences = await SharedPreferences.getInstance();
      setState(() {
        sso_id = prefs.getString('sso_id').toString();
        api_token = prefs.getString('api_token').toString();
      });
    })();
  }

  @override
  Widget build(BuildContext context) {
    return CurvedNavigationBar(
      key: _bottomNavigationKey,
      index: widget.idx,
      height: 60.0,
      items: <Widget>[
        Icon(
          Icons.home,
          size: 30,
          color: Colors.white,
        ),
        Icon(Icons.account_box, size: 30, color: Colors.white),
        Icon(Icons.directions_car, size: 30, color: Colors.white),
        // Icon(Icons.history, size: 30, color: Colors.white),
        Icon(Icons.playlist_add_check, size: 30, color: Colors.white),
      ],
      color: blue3,
      buttonBackgroundColor: blue3,
      backgroundColor: Colors.white,
      animationCurve: Curves.easeInOut,
      animationDuration: Duration(milliseconds: 600),
      onTap: (int index) {
        setState(() {
          _selectedIndex = index;
        });
        switch (index) {
          case 0:
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
              return new Home();
            }));
            break;
          case 1:
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
              return new Home();
            }));
            break;
          case 2:
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
              return new Home();
            }));
            break;
          case 3:
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
              return new Home();
            }));
            break;
        }
      },
    );
  }
}
