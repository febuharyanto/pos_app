import 'package:flutter/material.dart';

class InputText extends StatefulWidget {
  final String text;
  final TextEditingController controllerText;
  final String typeInputText;

  InputText(
      {required this.text,
      required this.controllerText,
      required this.typeInputText});

  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);
  bool _obscureText = true;
  final FocusNode _weightFocus = FocusNode();

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.typeInputText == 'password') {
      return TextFormField(
        textInputAction: TextInputAction.done,
        controller: widget.controllerText,
        // focusNode: _weightFocus,
        obscureText: _obscureText,
        onFieldSubmitted: (value) {
          _weightFocus.unfocus();
          // _prosesLogin();
        },
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Password",
            hintStyle: TextStyle(color: Colors.black),
            suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye, color: Colors.black),
                onPressed: _toggle),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.blue.shade800,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.black,
                width: 2.0,
              ),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      );
    } else {
      return TextFormField(
        controller: widget.controllerText,
        textInputAction: TextInputAction.next,
        obscureText: false,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: widget.text,
            hintStyle: TextStyle(color: Colors.black),
            fillColor: Colors.black,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.blue.shade800,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.black,
                width: 2.0,
              ),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      );
    }
  }
}
